﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.IO;

namespace ReportRequest
{

    class Program
    {

        static void Main(string[] args)
        {
            string path = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            char[] splitChars = { ',', '\n' };

            string reportCheckSQL = "select * from veritasreports.dbo.reportrequest where reportcomplete = " + 0 + " or reportemailed = " + 0;

            dBO.dboOpen(path);
            DataTable dt = dBO.dboInportInformation(reportCheckSQL);
            dBO.dboClose();

            foreach (DataRow dr in dt.Rows)
            {
                int requestID = (int)dr["idx"];
                string[] headers = dr["ReportHeader"].ToString().Split(splitChars);
                string userSQL = dr["ReportSQL"].ToString();
                string reportName = dr["ReportName"].ToString();
                string fileName = dr["ReportFile"].ToString().Remove(dr["ReportFile"].ToString().Length - 4) + "-" + DateTime.Now.ToString("MM-dd-yyyy_") + requestID + ".csv";
                string userEmail = dr["RequestByEmail"].ToString();
                string userName = dr["RequestByName"].ToString().Trim();
                DateTime requestedDate = (DateTime)dr["RequestDate"];
                bool reportComplete = (bool)dr["ReportComplete"];
                bool reportEMailed = (bool)dr["ReportEMailed"];

                if (!reportComplete)
                {
                    executeFileWrite(headers, userSQL, reportName, fileName, requestID);
                    moveFile(fileName);
                }
                if (!reportEMailed)
                {
                    executeEmail(userEmail, reportName, fileName, requestID);
                }
            }

            void executeFileWrite(string[] passedHeaders, string passedSQL, string passedReportName, string passedFileName, int reqID)
            {
                WriteToFile(passedHeaders, passedSQL, passedReportName, passedFileName);
                string updateSQL = "update veritasreports.dbo.reportrequest " +
                                   "set reportcomplete = " + 1 +
                                   " where idx = " + reqID;
                dBO.dboOpen(path);
                dBO.dboAlterDBOUnsafe(updateSQL);
                dBO.dboClose();
            }

            void moveFile(string passedFileName)
            {
                string tempFilePath = @"C:\RAA\ReportTemp\" + passedFileName;
                string correctFilePath = @"C:\inetpub\wwwroot\VeritasGenerator\wwwroot\Downloads\" + passedFileName;
                System.IO.File.Move(tempFilePath, correctFilePath);
            }

            void executeEmail(string passedEmail, string passedReportName, string passedFileName, int reqID)
            {
                sendEmail(passedEmail, passedReportName, passedFileName);
                string updateSQL = "update veritasreports.dbo.reportrequest " +
                            "set reportemailed = " + 1 +
                            " where idx = " + reqID;
                dBO.dboOpen(path);
                dBO.dboAlterDBOUnsafe(updateSQL);
                dBO.dboClose();
            }

            void WriteToFile(string[] passedUserHeaders, string passedUserSQL, string givenReportName, string givenFileName)
            {
                string tempFilePath = @"C:\RAA\ReportTemp\";
                string writeLine = "";
                CSharpDBO.CSharpDBO tdBO = new CSharpDBO.CSharpDBO();
                tdBO.dboOpen(path);
                DataTable tdt = tdBO.dboInportInformation(passedUserSQL);
                tdBO.dboClose();
                StreamWriter sr = new StreamWriter(tempFilePath + givenFileName);
                for (int i = 0; i < passedUserHeaders.Length; i++)
                {
                    passedUserHeaders[i] = passedUserHeaders[i].Trim();
                }
                foreach (string st in passedUserHeaders)
                {
                    writeLine = writeLine + st + ", ";
                }
                sr.WriteLine(writeLine);
                writeLine = "";

                foreach (DataRow drr in tdt.Rows)
                {
                    foreach (string str in passedUserHeaders)
                    {
                        writeLine = writeLine + drr[str].ToString().Replace(",", " ") + ",";
                    }
                    sr.WriteLine(writeLine);
                    writeLine = "";
                }
                sr.Close();
            }

            void sendEmail(string passedUserEmail, string requestedReportName, string requestedFileName)
            {
                try
                {
                    MailMessage message = new MailMessage();
                    SmtpClient smtp = new SmtpClient();
                    message.From = new MailAddress("info@veritasglobal.com");
                    message.To.Add(new MailAddress(passedUserEmail));
                    message.Subject = requestedReportName + " has been completed";
                    message.IsBodyHtml = false;
                    message.Body = "https://www.veritasgenerator.com/downloads/" + requestedFileName;
                    smtp.Port = 587;
                    smtp.Host = "smtp.office365.com";
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential("info@veritasglobal.com", "SilverSurfer3");
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}
